from setuptools import setup

import pkgconfig

setup(
    name='sao',
    version='0.1',
    scripts=[
        'sao/cli/cli_restart.py'
    ],
    packages=[
        'sao',
        'sao.problems',
        'sao.surrogates',
        'sao.algos',
        'sao.utils',
        'sao.ext',
        'sao.ext.bocs'
    ],
    install_requires=[
        'numpy',
        'sklearn',
        'scipy',
        'pandas',
        'matplotlib',
        'joblib',
        'cvxpy',
        'cvxopt'
    ]
)
