#!/usr/bin/env python3
''' Plot optim '''

import sys
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

if __name__ == '__main__':
    # data
    csv_files = sys.argv[1:]
    # plot
    fig = plt.figure(dpi=100, figsize=(4, 4))
    ax = fig.add_subplot(111)
    for csv in csv_files:
        df = pd.read_csv(csv, sep=';')
        group = df.groupby(['sample'])
        means = group.ybest.mean()
        confidences = (1.9 * group.ybest.std()) / np.sqrt(group.size())
        ax.plot(means)
        ax.fill_between(group.groups.keys(), means-confidences, means+confidences, alpha=.25)
    ax.grid()
    fig.savefig('out_optim.pdf', format='pdf', bbox_inches='tight')
