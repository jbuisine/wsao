#!/bin/sh

#OAR --array-param-file params.txt
#OAR -l /core=1,walltime=1:00:00
#OAR -p host="orval01"
#OAR -t besteffort
#OAR -t idempotent

. ~/opt/venvs/sao/bin/activate

OPENBLAS_NUM_THREADS=1 cli_restart.py $@ 2> /dev/null
