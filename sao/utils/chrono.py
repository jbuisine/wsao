''' Chrono '''

#######################################
# Chrono
#######################################

def chrono(f):
    def _f(*args,**kwargs):
        import time
        tic = time.time()
        f(*args,**kwargs)
        return time.time() - tic
    return _f
