''' Parser '''

import numpy as np

#######################################
# Parser
#######################################

def parse_value(string):
    if string == 'True':
        return True
    if string == 'False':
        return False
    try:
        return int(string)
    except ValueError:
        try:
            return float(string)
        except ValueError:
            return string

def parse_params(param_str):
    dict_params = {}
    if param_str != 'none':
        for key_value_str in param_str.split(','):
            key, value = key_value_str.split('=')
            dict_params[key] = parse_value(value)
    return dict_params
