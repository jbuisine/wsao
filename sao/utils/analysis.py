''' Analysis '''

import logging

import numpy as np
from sklearn.metrics import r2_score

#########################################
# Analysis
#########################################

class Analysis:
    def __init__(self, logfile, **_):
        logging.basicConfig(level=logging.INFO, format='%(msg)s', filename=logfile, filemode='w')

#########################################
# SamplerAnalysis
#########################################

class SamplerAnalysis(Analysis):
    def __init__(self, logfile, problem, sample_size=2000):
        super().__init__(logfile)
        # sample set
        self.xsample = np.random.randint(0, 2, (sample_size, problem.size))
        # log header
        logging.info('x;y')
        # log sample
        for x in self.xsample:
            logging.info('{};{}'.format(
                ','.join(list(map(str, x.tolist()))),
                problem.evaluate(x)
            ))

#########################################
# FitterAnalysis
#########################################

class FitterAnalysis(Analysis):
    def __init__(self, logfile, **_):
        super().__init__(logfile)
        # test set
        self.xtest = None
        self.ytest = None
        # log header
        logging.info('sample;mae;chrono')

    def testset(self, test):
        self.xtest = np.array([
            np.fromstring(x, dtype=int, sep=',')
            for x in test.x.values
        ])
        self.ytest = test.y.values

    def log(self, sample, chrono, surrogate):
        # compute mae
        mae = np.mean(np.abs(self.ytest - surrogate.predict(self.xtest)))
        logging.info('{};{};{}'.format(sample, mae, chrono))

    def coefficient_of_determination(self, surrogate):
        return r2_score(self.ytest, surrogate.predict(self.xtest))

    def mae(self, surrogate):
        return np.mean(np.abs(self.ytest - surrogate.predict(self.xtest)))

#########################################
# OptimizerAnalysis
#########################################

class OptimizerAnalysis(Analysis):
    def __init__(self, logfile, **_):
        super().__init__(logfile)
        # log header
        logging.info('sample;xbest;ybest')

    def log(self, sample, xbest, ybest):
        logging.info('{};{};{}'.format(sample, xbest.tolist(), ybest))
