''' Algo '''

import numpy

#########################################
# Algo
#########################################

class Algo:
    def __init__(self, problem, surrogate, analysis, seed=0):
        self.problem = problem
        self.surrogate = surrogate
        self.analysis = analysis
        self.seed = seed
        numpy.random.seed(seed)
