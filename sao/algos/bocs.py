''' BOCS '''

import numpy as np

from ..ext.bocs.BOCS import BOCS
from ..ext.bocs.sample_models import sample_models

from .optimizer import OptimizerAlgo

#######################################
# BOCS
#######################################

class BayesianOptimizationCombinatorialSpace(OptimizerAlgo):
    def run(self, initial_sample=1, max_sample=1, restarts=1, iterations=1, order=2, acquisitionFn='SA'):
        inputs = {}
        inputs['n_vars'] = self.problem.size
        inputs['n_init'] = initial_sample
        inputs['evalBudget'] = max_sample
        inputs['model'] = self.problem.evaluate_
        inputs['penalty'] = lambda _: 0
        inputs['restarts'] = restarts
        inputs['iterations'] = iterations
        inputs['x_vals'] = sample_models(inputs['n_init'], inputs['n_vars'])
        inputs['y_vals'] = inputs['model'](inputs['x_vals'])
        inputs['target'] = self.target
        inputs['analysis'] = self.analysis
        BOCS(inputs, order, acquisitionFn)
