''' Exhaustive '''

import numpy as np

from .optimizer import OptimizerAlgo

#######################################
# Exhaustive
#######################################

class Exhaustive(OptimizerAlgo):
    def optimize(self, *_):
        # initialization
        xbest, ybest = (None, self.target.bound)
        # exhaustive search
        binary = '{0:0' + str(self.problem.size) + 'b}'
        for n in range(2**self.problem.size):
            x = np.array(list(binary.format(n)), dtype='i')
            y = self.surrogate.predict([x])
            if self.target.isbetter(y, ybest):
                xbest, ybest = x, y
        # best
        return xbest
