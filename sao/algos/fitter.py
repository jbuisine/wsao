''' Fitter '''

import numpy as np
import pandas as pd

from .algo import Algo

#######################################
# Fitter
#######################################

class FitterAlgo(Algo):
    def run(self, samplefile='out_sample.csv', sample=1, step=1):
        # data set
        df = pd.read_csv(samplefile, sep=';')
        # learning set and test set
        learn = df.sample(sample)
        test = df.drop(learn.index)
        self.analysis.testset(test)
        # fit
        for s, (x, y) in enumerate(zip(learn.x.values, learn.y.values)):
            self.surrogate.addsample(
                np.fromstring(x, dtype=int, sep=','),
                y
            )
            if s % step == 0:
                self.analysis.log(s, self.surrogate.fit(), self.surrogate)

    # specific method where learn and test set are passed
    def run_samples(self, learn, test, step=1):
        
        self.analysis.testset(test)
        # fit
        for s, (x, y) in enumerate(zip(learn.x.values, learn.y.values)):
            self.surrogate.addsample(
                np.fromstring(x, dtype=int, sep=','),
                y
            )
            if s % step == 0:
                self.analysis.log(s, self.surrogate.fit(), self.surrogate)
        
