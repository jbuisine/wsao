''' IncrementalEval '''

import numpy as np

from .optimizer import OptimizerAlgo

#######################################
# IncrementalEval
#######################################

class IncrementalEval(OptimizerAlgo):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.graph = None
        self.map = None
        self.bases = None
        self.deltas = None

    def store_graph(self):
        return np.array([
            np.unique(
                np.concatenate([[]] + [
                    interaction[interaction != n]
                    for interaction in self.surrogate.interactions
                    if n in interaction
                ])
            )
            for n in range(self.problem.size)
        ])

    def store_map(self):
        m = [{n: set([])} for n in range(self.problem.size)]
        for n, interaction in enumerate(self.surrogate.interactions):
            for major in interaction:
                if major in m[major]:
                    m[major][major].add(n)
                else:
                    m[major][major] = set([n])
                for minor in interaction:
                    if minor in m[major]:
                        m[major][minor].add(n)
                    else:
                        m[major][minor] = set([n])
        return m

    def store_bases(self, x):
        return np.array([
            self.surrogate.evaluate_basis(x, interaction)
            for interaction in self.surrogate.interactions
        ])

    def store_deltas(self):
        return np.array([
            self.surrogate.delta(
                self.bases, self.map[move][move]
            )
            for move in range(self.problem.size)
        ])

    def update_bases(self, x, argbest):
        for k in self.map[argbest][argbest]:
            self.bases[k] = self.surrogate.evaluate_basis(
                x, self.surrogate.interactions[k]
            )

    def restart(self):
        x = np.random.randint(0, 2, self.problem.size)
        y = self.surrogate.predict([x])[0]
        return x, y

    def optimize(self, restarts, iterations):
        # initialization
        xbest, ybest = (None, self.target.bound)
        self.graph = self.store_graph()
        self.map = self.store_map()
        # restarts
        for r in range(restarts):
            x, y = self.restart()
            self.bases = self.store_bases(x)
            self.deltas = self.store_deltas()
            # iterations
            for i in range(iterations):
                argbest = self.target.argbest(self.deltas)
                if not self.target.isbetter(self.deltas[argbest], 0):
                    break
                # update current solution
                x = self.bitflip(x, argbest)
                y += self.deltas[argbest]
                # update stored data
                self.update_bases(x, argbest)
                self.update_deltas(argbest)
            # update best
            if self.target.isbetter(y, ybest):
                xbest, ybest = x, y
        return xbest

#######################################
# SimpleIncrementalEval
#######################################

class SimpleIncrementalEval(IncrementalEval):
    def update_deltas(self, *_):
        self.deltas = self.store_deltas()

#######################################
# DoubleIncrementalEval
#######################################

class DoubleIncrementalEval(IncrementalEval):
    def update_deltas(self, argbest):
        for (variable, interactions) in self.map[argbest].items():
            self.deltas[variable] += self.surrogate.delta2(
                self.bases, interactions
            )
