''' Optimizer '''

import numpy as np

from .algo import Algo

#######################################
# Optimizer
#######################################

class Target:
    def __init__(self, target):
        if target == 'min':
            self.best = 'min'
            self.bound = np.inf
            self.argbest = np.argmin
        else:
            self.best = 'max'
            self.bound = -np.inf
            self.argbest = np.argmax

    def isbetter(self, a, b):
        return (a < b) if self.best == 'min' else (a > b)

class OptimizerAlgo(Algo):
    def __init__(self, problem, surrogate, analysis, seed=0, target='min'):
        super().__init__(problem, surrogate, analysis, seed)
        self.target = Target(target)
        self.best = (None, self.target.bound)

    def bitflip(self, x, flip):
        _x = np.array(x)
        _x[flip] ^= 1
        return _x

    def updatebest(self, x, y):
        bestx, besty = self.best
        if self.target.isbetter(y, besty):
            self.best = (x, y)

    def run(self, initial_sample=1, max_sample=1, restarts=1, iterations=1):
        # initial sample
        for s in range(initial_sample):
            x = np.random.randint(0, 2, self.problem.size)
            self.surrogate.addsample(x, self.problem.evaluate(x))
        # optimization
        for s in range(initial_sample, max_sample):
            # learn
            self.surrogate.fit()
            # search for a promising x
            x = self.optimize(restarts, iterations)
            # sample randomly if x is already known
            if any(np.equal(self.surrogate.sample, x).all(1)):
                x = np.random.randint(0, 2, self.problem.size)
            # evaluate
            y = self.problem.evaluate(x)
            # add to sample
            self.surrogate.addsample(x, y)
            # update best
            self.updatebest(x, y)
            # log best
            self.analysis.log(s, self.best[0], self.best[1])
