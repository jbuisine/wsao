''' Efficient Global Optimization '''

import numpy as np

from scipy.stats import norm

from .optimizer import OptimizerAlgo

#######################################
# EfficientGlobalOptimization
#######################################

class EfficientGlobalOptimization(OptimizerAlgo):
    def expected_improvement(self, x):
        y, std = self.surrogate.predict([x], return_std=True)
        if std[0] == 0:
            return 0
        delta = y[0] - self.surrogate.ymax
        return 10**(delta * norm.cdf(delta / std[0])
                    + std[0] * norm.pdf(delta / std[0]))

    def optimize(self, restarts, iterations):
        # initialization
        xbest, eibest = (None, self.target.bound)
        # restarts
        for r in range(restarts):
            x = np.random.randint(0, 2, self.problem.size)
            ei = self.expected_improvement(x)
            # iterations
            for i in range(iterations):
                x_ = self.bitflip(x, np.random.randint(x.size))
                ei_ = self.expected_improvement(x_)
                if ei_ > ei:
                    x, ei = x_, ei_
            # update best
            if self.target.isbetter(ei, eibest):
                xbest, eibest = x, ei
        return xbest
