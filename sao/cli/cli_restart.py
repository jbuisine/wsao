#!/usr/bin/env python3

import sys

from sao.portfolio import Algos, Analysis, Models, Problems, Surrogates
from sao.utils.parser import parse_params

if __name__ == '__main__':

    # command line arguments
    if len(sys.argv) != 7:
        print('usage: ', sys.argv[0], '<problem> <model> <surrogate> <algo> <run> <logs>')
        sys.exit(-1)

    # parse params
    PROBLEM_PARAMS = parse_params(sys.argv[1])
    MODEL_PARAMS = parse_params(sys.argv[2])
    SURROGATE_PARAMS = parse_params(sys.argv[3])
    ALGO_PARAMS = parse_params(sys.argv[4])
    RUN_PARAMS = parse_params(sys.argv[5])
    LOGS_PARAMS = parse_params(sys.argv[6])

    # extract params
    PROBLEM = PROBLEM_PARAMS.pop('problem', 'nkl')
    MODEL = MODEL_PARAMS.pop('model', 'gaussianprocess')
    SURROGATE = SURROGATE_PARAMS.pop('surrogate', 'gaussianprocess')
    ALGO = ALGO_PARAMS.pop('algo', 'fitter')
    ALGO_SEED = ALGO_PARAMS.pop('seed', 0)
    ALGO_RESTARTS = ALGO_PARAMS.pop('algo_restarts', 0)
    LOGS_ANALYSIS = LOGS_PARAMS.pop('analysis', 'sampler')

    # init problem
    P = Problems[PROBLEM](**PROBLEM_PARAMS)
    Z = Analysis[LOGS_ANALYSIS](problem=P, **LOGS_PARAMS)

    # restart
    for r in range(ALGO_RESTARTS):
        # init components
        M = Models[MODEL](**MODEL_PARAMS)
        S = Surrogates[SURROGATE](size=P.size, model=M, **SURROGATE_PARAMS)
        A = Algos[ALGO](problem=P, surrogate=S, analysis=Z, seed=ALGO_SEED+r, **ALGO_PARAMS)
        # run
        A.run(**RUN_PARAMS)
