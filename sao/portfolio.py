''' SaO Portolio '''

from sklearn.linear_model import (LinearRegression, Lasso, Lars, LassoLars,
                                    LassoCV, ElasticNet)

from .algos.fitter import FitterAlgo
from .algos.incrementaleval import SimpleIncrementalEval, DoubleIncrementalEval
from .algos.ego import EfficientGlobalOptimization
from .algos.bocs import BayesianOptimizationCombinatorialSpace
from .algos.exhaustive import Exhaustive
from .problems.ubqp import UBQPProblem
from .problems.nklandscape import NKLandscapeProblem
from .problems.nd3dproblem import ND3DProblem
from .surrogates.multilinear import MultilinearSurrogate
from .surrogates.walsh import WalshSurrogate
from .surrogates.gaussianprocess import (CombinatorialGaussianProcessRegressor,
                                            GaussianProcessSurrogate)
from .utils.analysis import SamplerAnalysis, FitterAnalysis, OptimizerAnalysis

Algos = {
    'fitter': FitterAlgo,
    'sie': SimpleIncrementalEval,
    'die': DoubleIncrementalEval,
    'ego': EfficientGlobalOptimization,
    'bocs': BayesianOptimizationCombinatorialSpace,
    'exhaustive': Exhaustive
}

Analysis = {
    'sampler': SamplerAnalysis,
    'fitter': FitterAnalysis,
    'optimizer': OptimizerAnalysis
}

Models = {
    'lin': LinearRegression,
    'lasso': Lasso,
    'lars': Lars,
    'lassolars': LassoLars,
    'lassocv': LassoCV,
    'gaussianprocess': CombinatorialGaussianProcessRegressor,
    'elasticnet': ElasticNet
}

Problems = {
    'nkl': NKLandscapeProblem,
    'ubqp': UBQPProblem,
    'nd3d': ND3DProblem
}

Surrogates = {
    'walsh': WalshSurrogate,
    'multilinear': MultilinearSurrogate,
    'gaussianprocess': GaussianProcessSurrogate
}
