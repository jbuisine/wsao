''' Unconstrained Binary Quadratic Problem (UBQP) '''

import numpy as np

from .problem import Problem

#########################################
# UBQP
#########################################

class UBQPProblem(Problem):
    def __init__(self, size, d, seed=0):
        super().__init__(size, seed)
        # density
        self.d = d
        # matrix
        q = np.array([
                np.array([
                    0
                    if np.random.rand() < d
                    else np.random.randint(-100, 100)
                    for _ in range(size)
                ])
                for _ in range(size)
            ])
        self.q = ((q + q.T) / 2).astype(int)

    def evaluate(self, x):
        return sum([
            self.q[i,j]*x[i]*x[j]
            for i in range(self.size)
            for j in range(self.size)
        ])

    def __str__(self):
        return 'size:{},\nq:\n{}'.format(self.size, self.q)
