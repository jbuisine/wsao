''' NK Landscapes '''

import numpy as np

from .problem import Problem

#########################################
# NK landscapes
#########################################

class NKLandscapeProblem(Problem):

    def __init__(self, size, k, seed=0):
        super().__init__(size, seed)
        # number of interactions
        self.k = k
        # contribution functions
        self.g = np.random.rand(size, 2**(k+1))
        # interactions
        serie = np.arange(size)
        self.interactions = np.array([
            np.concatenate((
                [_],
                np.random.choice(
                    np.concatenate((serie[:_], serie[_+1:])), k, replace=False
                )
            ))
            for _ in serie
        ])

    def evaluate(self, x):
        # convert x to string
        xstr = np.char.mod('%d', x)
        # mean of contribution functions
        return np.mean([
            self.g[_, int(''.join(xstr[self.interactions[_]]), 2)]
            for _ in range(self.size)
        ])

    def __str__(self):
        return 'size:{}, k:{}\ninteractions:\n{}\ng:\n{}'.format(
            self.size, self.k,
            '\n'.join(map(str, self.interactions)),
            '\n'.join(map(str, self.g)))
