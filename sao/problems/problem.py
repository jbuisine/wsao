''' Problem '''

import numpy

#########################################
# Problem
#########################################

class Problem:
    def __init__(self, size, seed):
        self.size = size
        self.seed = seed
        numpy.random.seed(seed)

    def evaluate_(self, x):
        return numpy.array([self.evaluate(_) for _ in x])
