import pandas as pd
from sklearn.utils import shuffle
from sklearn.metrics import roc_auc_score

from sklearn.model_selection import GridSearchCV
import sklearn.svm as svm
import datetime

from .problem import Problem

class ND3DProblem(Problem):
    
    # def _get_best_model(self, X_train, y_train):

    #     Cs = [0.001, 0.01, 0.1, 1, 10, 100, 1000]
    #     gammas = [0.001, 0.01, 0.1, 5, 10, 100]
    #     param_grid = {'kernel':['rbf'], 'C': Cs, 'gamma' : gammas}

    #     svc = svm.SVC(probability=True, class_weight='balanced')
    #     #clf = GridSearchCV(svc, param_grid, cv=5, verbose=1, scoring=my_accuracy_scorer, n_jobs=-1)
    #     clf = GridSearchCV(svc, param_grid, cv=5, verbose=1, n_jobs=-1)

    #     clf.fit(X_train, y_train)

    #     model = clf.best_estimator_

    #     return model

    # def svm_model(self, X_train, y_train):

    #     return self._get_best_model(X_train, y_train)
        
    def __init__(self, size, seed=0):
        super().__init__(size, seed)

        ########################
        # 1. Get and prepare data
        ########################
        # dataset_train = pd.read_csv(filename + '.train', header=None, sep=";")
        # dataset_test = pd.read_csv(filename + '.test', header=None, sep=";")

        # # default first shuffle of data
        # dataset_train = shuffle(dataset_train)
        # dataset_test = shuffle(dataset_test)

        # # get dataset with equal number of classes occurences
        # noisy_df_train = dataset_train[dataset_train.iloc[:, 0] == 1]
        # not_noisy_df_train = dataset_train[dataset_train.iloc[:, 0] == 0]
        # #nb_noisy_train = len(noisy_df_train.index)

        # noisy_df_test = dataset_test[dataset_test.iloc[:, 0] == 1]
        # not_noisy_df_test = dataset_test[dataset_test.iloc[:, 0] == 0]
        # #nb_noisy_test = len(noisy_df_test.index)

        # # use of all data
        # final_df_train = pd.concat([not_noisy_df_train, noisy_df_train])
        # final_df_test = pd.concat([not_noisy_df_test, noisy_df_test])

        # # shuffle data another time
        # final_df_train = shuffle(final_df_train)
        # final_df_test = shuffle(final_df_test)

        # # use of the whole data set for training
        # x_dataset_train = final_df_train.iloc[:,1:]
        # x_dataset_test = final_df_test.iloc[:,1:]

        # y_dataset_train = final_df_train.iloc[:,0]
        # y_dataset_test = final_df_test.iloc[:,0]

        # self.x_train = x_dataset_train
        # self.y_train = y_dataset_train
        # self.x_test = x_dataset_test
        # self.y_test = y_dataset_test

    # def evaluate(self, solution):
        
        # start = datetime.datetime.now()
        # # get indices of filters data to use (filters selection from solution)
        # indices = []

        # for index, value in enumerate(solution.data): 
        #     if value == 1: 
        #         indices.append(index) 

        # # keep only selected filters from solution
        # x_train_filters = self.x_train.iloc[:, indices]
        # y_train_filters = self.y_train
        # x_test_filters = self.x_test.iloc[:, indices]

        # # TODO : use of GPU implementation of SVM
        # model = self.svm_model(x_train_filters, y_train_filters)
        
        # y_test_model = model.predict(x_test_filters)
        # test_roc_auc = roc_auc_score(self.y_test, y_test_model)

        # end = datetime.datetime.now()

        # diff = end - start

        # print("Evaluation took :", divmod(diff.days * 86400 + diff.seconds, 60))

        # return test_roc_auc

    def __str__(self):
        return "SAO ND3D attributes"