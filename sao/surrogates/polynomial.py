''' Polynomial Surrogate '''

import numpy as np

from itertools import combinations

from .surrogate import Surrogate

#########################################
# Polynomial Surrogate
#########################################

class PolynomialSurrogate(Surrogate):
    def __init__(self, size, order, model):
        super().__init__(size, model)
        # contributions
        self.contributions = np.array([
            np.array(p)
            for o in range(1, order+1)
            for p in combinations(range(size), o)
        ])
        # reshape train
        self.train = self.train.reshape((0, len(self.contributions)))
        # polynomial
        self.coefficients = None
        self.interactions = None

    def addsample(self, x, y):
        # sample
        self.sample = np.append(self.sample, [x], axis=0)
        # compute predictors
        self.train = np.append(self.train, [[
            self.evaluate_basis(x, c)
            for c in self.contributions
        ]], axis=0)
        # target
        self.targets = np.append(self.targets, y)
        if y > self.ymax:
            self.ymax = y
        if y < self.ymin:
            self.ymin = y

    def predict(self, x):
        return np.array([
            np.sum([
                w * self.evaluate_basis(xx, c)
                for w, c in zip(self.coefficients, self.interactions)
            ]) + self.model.intercept_
            for xx in x
        ])

    def fit(self):
        super().fit()
        # clean ineffective terms
        self.interactions = self.contributions[self.model.coef_ != 0]
        self.coefficients = self.model.coef_[self.model.coef_ != 0]
