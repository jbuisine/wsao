''' Surrogate '''

import numpy as np

from ..utils.chrono import chrono

#########################################
# Surrogate
#########################################

class Surrogate:
    def __init__(self, size, model):
        self.size = size
        self.model = model
        self.sample = np.empty(shape=(0, size), dtype='i')
        self.train = np.empty(shape=(0, size), dtype='i')
        self.targets = np.empty(shape=(0), dtype='i')
        self.ymax = -np.inf
        self.ymin = np.inf

    def addsample(self, x, y):
        self.sample = np.append(self.sample, [x], axis=0)
        self.train = np.append(self.train, [x], axis=0)
        self.targets = np.append(self.targets, y)
        if y > self.ymax:
            self.ymax = y
        if y < self.ymin:
            self.ymin = y

    def predict(self, x):
        return self.model.predict(x)

    @chrono
    def fit(self):
        self.model.fit(self.train, self.targets)
