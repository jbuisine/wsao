''' Walsh Surrogate '''

import numpy as np

from .polynomial import PolynomialSurrogate

#########################################
# Walsh Surrogate
#########################################

class WalshSurrogate(PolynomialSurrogate):
    def evaluate_basis(self, x, contributions):
        return -1 if np.count_nonzero(x[contributions]) & 0x1 else 1

    def delta(self, bases, interactions):
        return np.sum(
            np.array([
                self.coefficients[k] * bases[k]
                for k in interactions
            ]) * -2
        )

    def delta2(self, bases, interactions):
        return 2 * self.delta(bases, interactions)
