''' GaussianProcess '''

import numpy as np

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import PairwiseKernel

from .surrogate import Surrogate

#########################################
# Combinatorial GaussianProcessRegressor
#########################################

def hamming(x1, x2, gamma):
    return np.exp(-gamma * np.count_nonzero(x1 != x2))

def CombinatorialGaussianProcessRegressor(**params):
    kernel = PairwiseKernel(metric=hamming, gamma=1e-2)
    return GaussianProcessRegressor(kernel=kernel, optimizer=None, **params)

#########################################
# GaussianProcess Surrogate
#########################################

class GaussianProcessSurrogate(Surrogate):
    def predict(self, x, return_std=False):
        return self.model.predict(x, return_std=return_std)
