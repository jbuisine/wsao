''' Mutilinear Surrogate '''

import numpy as np

from .polynomial import PolynomialSurrogate

#########################################
# Multilinear Surrogate
#########################################

class MultilinearSurrogate(PolynomialSurrogate):
    def evaluate_basis(self, x, contributions):
        return 1 if np.count_nonzero(x[contributions]) == contributions.size else 0

    def delta(self, bases, interactions):
        return np.sum(
            np.array([
                self.coefficients[k] * bases[k]
                for k in interactions
            ]) * -1
        )

    def delta2(self, bases, interactions):
        raise NotImplementedError
