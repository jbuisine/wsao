# WSaO

Walsh Surrogate-assisted Optimization framework.

## What is WSaO ?

WSaO is the first surrogate-assisted optimization algorithm based on the
mathematical foundations of discrete Walsh functions, combined with the powerful
grey-box optimization techniques in order to solve pseudo-boolean optimization
problems.

For more details, please see [Walsh functions as surrogate model for pseudo-boolean optimization problems](https://hal.archives-ouvertes.fr/hal-02190141/document), published in The Genetic and Evolutionary Computation Conference (GECCO), 2019.

## Authors

[Florian Leprêtre](https://www-lisic.univ-littoral.fr/~flepretre/),
[Cyril Fonlupt](https://www-lisic.univ-littoral.fr/~fonlupt/),
[Sébastien Verel](https://www-lisic.univ-littoral.fr/~verel/),
[Virginie Marion](http://www-lisic.univ-littoral.fr/~poty/)

Université du Littoral Côte d'Opale, Calais, France

## Setup

The use of a python [virtual environment](https://virtualenv.pypa.io/en/latest/) is recommended.

```sh
git clone https://gitlab.com/florianlprt/wsao.git
cd wsao
# activate your python virtual environment, then
pip install pkgconfig setuptools
pip install . --upgrade
```

## Usage

### Command line

```sh
cli_restart.py <problem> <model> <surrogate> <algorithm> <run> <analysis>
```

### Parameters

**`problem`:** Set up the pseudo-boolean problem
- `nkl`: NK-Landscapes
    - `size`: number of variables
    - `k`: number of epistasic interactions per variable
- `ubqp`: Unconstrained Binary Quadratic Programming
    - `size`: number of variables
    - `d`: density of zeros in the *Q* matrix

**`model`:** Set up the regression model (from [scikit-learn](https://scikit-learn.org/stable/)); each should be further parametrized according to their respective documentation
- `lin`: linear model ([→](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html))
- `lasso`: linear model with L1 prior as regularizer ([→](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html))
- `lars`: least angle regression ([→](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lars.html))
- `lassolars`: lasso model fit with lars ([→](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LassoLars.html))
- `gaussianprocess`: gaussian process regression ([→](https://scikit-learn.org/stable/modules/generated/sklearn.gaussian_process.GaussianProcessRegressor.html))

**`surrogate`:** Set up the surrogate model
- `multilinear`: multilinear polynomial basis
- `walsh`: Walsh functions basis
- `gaussianprocess`: combinatorial gaussian process

**`algorithm`:** Set up the algorithm to be executed
- `fitter`: Compute the accuracy of surrogate models (mae)
- `exhaustive`: Exhaustive optimization
- `sie`: Simple Incremental Evaluation
- `die`: Double Incremental Evaluation
- `bocs`: Bayesian Optimization of Combinatorial Structures ([BOCS](https://github.com/baptistar/BOCS))
- `ego`: Efficient Global Optimization for Combinatorial Problems ([EGO](http://www.spotseven.de/wp-content/papercite-data/pdf/zaef14b.pdf))


**`run`:** Set up specific run parameters

**`analysis`:** Set up the output

### Examples

#### Sampling examples

Sample `sample_size` example points in the search space according to a `problem`
and store them in a `logfile`.

The `model`, `surrogate`, `algorithm` and `run` parameters are set to `none` as
they are not usefull in this context.

```sh
cli_restart.py problem=nkl,size=10,k=1 \
               none \
               none \
               none \
               none \
               analysis=sampler,sample_size=10000,logfile=out_sample.csv
```

#### Fitting models

Compute the accuracy of a Walsh surrogate model trained with Lasso regressor on a NK-Landscape problem.

```sh
cli_restart.py problem=nkl,size=10,k=1 \
               model=lasso,alpha=1e-5 \
               surrogate=walsh,order=2 \
               algo=fitter,algo_restarts=10 \
               sample=100,step=10 \
               analysis=fitter,logfile=out_fit.csv,testfile=out_sample.csv
```

#### Optimization

Maximize NK-Landscape problem with a Surrogate-assisted Optimization approach
(WSaO).

```sh
cli_restart.py problem=nkl,size=10,k=1 \
               model=lasso,alpha=1e-5 \
               surrogate=walsh,order=2 \
               algo=die,target=max,algo_restarts=10 \
               initial_sample=10,max_sample=100,restarts=100,iterations=100 \
               analysis=optimizer,logfile=out_optim.csv
```

#### Plots

Plot mean absolute error or optimization.

```sh
python analysis/plot_mae.py out_fit.csv # [out_fit2.csv out_fit3.csv ...]
python analysis/plot_optim.py out_optim.csv # [out_optim2.csv out_optim3.csv ...]
```

#### More

More experiments are available in the run/params.txt file.
